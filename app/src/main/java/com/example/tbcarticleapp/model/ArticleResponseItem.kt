package com.example.tbcarticleapp.model

data class ArticleResponseItem(
    val events: List<Events>,
    val featured: Boolean,
    val id: Int,
    val imageUrl: String,
    val newsSite: String,
    val publishedAt: String,
    val summary: String,
    val title: String,
    val updatedAt: String,
    val url: String
)
data class Events(
    val id: Int,
    val provider:String
)