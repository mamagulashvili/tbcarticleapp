package com.example.tbcarticleapp.ui.fragment

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.tbcarticleapp.api.RetrofitService
import com.example.tbcarticleapp.model.ArticleResponseItem
import com.example.tbcarticleapp.util.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ArticleViewModel : ViewModel() {


    val article: MutableLiveData<Resource<List<ArticleResponseItem>>> = MutableLiveData()

    fun getArticles() = viewModelScope.launch(Dispatchers.IO) {
        article.postValue(Resource.Loading())
        val response = RetrofitService.articleApi.getArticle()
        if (response.isSuccessful) {
            response.body()?.let {
                article.postValue(Resource.Success(it))
            }
        } else {
            article.postValue(Resource.Error(response.errorBody().toString()))
        }
    }

    val searchArticle: MutableLiveData<Resource<List<ArticleResponseItem>>> = MutableLiveData()
    fun searchArticle(query: String) = viewModelScope.launch(Dispatchers.IO) {
        searchArticle.postValue(Resource.Loading())
        val response = RetrofitService.articleApi.searchArticle(query)
        if (response.isSuccessful) {
            response.body()?.let {
                searchArticle.postValue(Resource.Success(it))
            }
        } else {
            searchArticle.postValue(Resource.Error(response.errorBody().toString()))
        }
    }
}