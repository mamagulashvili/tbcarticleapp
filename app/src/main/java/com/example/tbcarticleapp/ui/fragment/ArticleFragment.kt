package com.example.tbcarticleapp.ui.fragment

import android.util.Log.d
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.tbcarticleapp.R
import com.example.tbcarticleapp.adapter.ArticleAdapter
import com.example.tbcarticleapp.adapter.DetailAdapter
import com.example.tbcarticleapp.base.BaseFragment
import com.example.tbcarticleapp.databinding.ArticleFragmentBinding
import com.example.tbcarticleapp.extensions.hide
import com.example.tbcarticleapp.extensions.show
import com.example.tbcarticleapp.model.ArticleResponseItem
import com.example.tbcarticleapp.util.Resource
import kotlinx.coroutines.launch

class ArticleFragment :
    BaseFragment<ArticleFragmentBinding>(ArticleFragmentBinding::inflate) {

    private val articleAdapter: ArticleAdapter by lazy { ArticleAdapter() }
    private val detailAdapter: DetailAdapter by lazy { DetailAdapter() }
    private val viewModel: ArticleViewModel by viewModels()
    private val detailList = mutableListOf<ArticleResponseItem>()

    override fun start(layoutInflater: LayoutInflater, viewGroup: ViewGroup?) {
        init()
    }

    private fun init() {
        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.getArticles()
        }
        observeArticle()
        setRecycleView()
        onArticleClick()
        setHasOptionsMenu(true)
        observeSearchArticle()
    }

    private fun setRecycleView() {
        binding.apply {
            rvArticles.apply {
                layoutManager =
                    LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
                adapter = articleAdapter
            }
        }
    }

    private fun observeArticle() {

        viewModel.article.observe(viewLifecycleOwner, { response ->
            when (response) {
                is Resource.Success -> {
                    binding.progressBar.hide()
                    response.data?.let {
                        articleAdapter.differ.submitList(it)
                    }
                }
                is Resource.Error -> {
                    binding.progressBar.hide()
                    d("Error", response.errorMessage.toString())
                }
                is Resource.Loading -> {
                    binding.progressBar.show()
                }

            }
        })
    }

    private fun setDetailRecycle() {
        binding.rvDescription.apply {
            this.isVisible = true
            layoutManager = LinearLayoutManager(requireContext())
            adapter = detailAdapter
            isNestedScrollingEnabled = false
        }
    }

    private fun onArticleClick() {
        articleAdapter.setOnItemClick {
            binding.progressBar.show()
            detailList.add(it)
            detailAdapter.setData(detailList)
            setDetailRecycle()
            binding.rvDescription.show()
            binding.progressBar.hide()
            detailList.clear()
        }

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu, menu)

        val searchItem = menu.findItem(R.id.menu_search)
        val searchView = searchItem.actionView as SearchView
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                if (query != null) {
                    binding.rvDescription.hide()
                    binding.rvArticles.scrollToPosition(0)
                    viewModel.searchArticle(query)
                    searchView.clearFocus()
                }
                return true
            }

            override fun onQueryTextChange(query: String?): Boolean {
                return true
            }

        })
    }

    private fun observeSearchArticle() {
        viewModel.searchArticle.observe(viewLifecycleOwner, { response ->
            when (response) {
                is Resource.Success -> {
                    binding.progressBar.hide()
                    response.data?.let {
                        articleAdapter.differ.submitList(it)
                    }
                }
                is Resource.Error -> {
                    binding.progressBar.hide()
                    d("Error", response.errorMessage.toString())
                }
                is Resource.Loading -> {
                    binding.progressBar.show()
                }

            }
        })
    }
}