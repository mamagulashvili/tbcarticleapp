package com.example.tbcarticleapp.extensions

import android.widget.ProgressBar
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.tbcarticleapp.R

fun AppCompatImageView.setImage(imageUrl: String) {
    Glide.with(this.context).load(imageUrl).placeholder(R.drawable.default_article_image).into(this)
}
fun ProgressBar.hide(){
    this.isVisible = false
}
fun ProgressBar.show(){
    this.isVisible = true
}
fun RecyclerView.show(){
    this.isVisible = true
}
fun RecyclerView.hide(){
    this.isVisible = false
}