package com.example.tbcarticleapp.api

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitService {
    private const val BASE_URL = "https://api.spaceflightnewsapi.net/"
    private val retrofit by lazy {
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }
    val articleApi: ArticleApi by lazy {
        retrofit.create(ArticleApi::class.java)
    }
}