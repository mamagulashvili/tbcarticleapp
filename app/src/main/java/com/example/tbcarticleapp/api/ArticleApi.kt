package com.example.tbcarticleapp.api

import com.example.tbcarticleapp.model.ArticleResponseItem
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ArticleApi {
    @GET("/v3/articles?")
    suspend fun getArticle(
        @Query("_limit")
        itemLimit:Int = 50,
        @Query("_sort")
        sort:String = "id",
        @Query("_start")
        startId:Int = 4900
    ):Response<List<ArticleResponseItem>>
    @GET("v3/articles?")
    suspend fun searchArticle(
        @Query("_q")
        query:String,
        @Query("_sort")
        sort: String = "id"
    ):Response<List<ArticleResponseItem>>
}