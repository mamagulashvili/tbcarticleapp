package com.example.tbcarticleapp.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding


typealias Inflate<T> = (LayoutInflater, ViewGroup?, Boolean) -> T

abstract class BaseFragment<VB : ViewBinding>(
    private val inflate: Inflate<VB>,
) : Fragment() {
    private var _binding: VB? = null
    protected val binding get() = _binding!!


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (_binding == null) {
            _binding = inflate(layoutInflater, container, false)
            start(inflater, container)
        }
        return binding.root
    }

    abstract fun start(layoutInflater: LayoutInflater, viewGroup: ViewGroup?)

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}