package com.example.tbcarticleapp.pagingresource

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.example.tbcarticleapp.api.RetrofitService
import com.example.tbcarticleapp.model.ArticleResponseItem

class ArticlePagingSource : PagingSource<Int, ArticleResponseItem>() {
    override fun getRefreshKey(state: PagingState<Int, ArticleResponseItem>): Int? {
        return state.anchorPosition
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, ArticleResponseItem> {
        try {
            val currentPage = params.key ?: 1
            val response = RetrofitService.articleApi.getArticle()
            val dataList = mutableListOf<ArticleResponseItem>()
            val data = response.body() ?: emptyList()
            dataList.addAll(data)

            val prevKey = if (currentPage == 1) null else currentPage - 1
            val nextKey = if (data.isEmpty()) null else currentPage + 1

            return LoadResult.Page(
                dataList, prevKey, nextKey
            )
        } catch (e: Exception) {
            return LoadResult.Error(e)
        }
    }

}