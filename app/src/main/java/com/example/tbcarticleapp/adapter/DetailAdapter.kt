package com.example.tbcarticleapp.adapter

import android.util.Log.d
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.tbcarticleapp.R
import com.example.tbcarticleapp.databinding.RowDetailItemBinding
import com.example.tbcarticleapp.model.ArticleResponseItem

class DetailAdapter : RecyclerView.Adapter<DetailAdapter.DetailViewHolder>() {

    val itemList = mutableListOf<ArticleResponseItem>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DetailViewHolder {
        return DetailViewHolder(
            RowDetailItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: DetailViewHolder, position: Int) {
        holder.onBind()
    }

    override fun getItemCount(): Int = 2

    inner class DetailViewHolder(val binding: RowDetailItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        lateinit var model: ArticleResponseItem
        fun onBind() {
            model = itemList[0]
            d("SIZE","$model")
            when (absoluteAdapterPosition) {
                0 -> {
                    binding.apply {
                        tvDescription.text = model.summary
                        tvTitle.text = "Description"
                    }
                }
                1 -> {
                    binding.apply {
                        tvTitle.text = "TimeLine"
                        val desc = StringBuilder()
                        for (i in model.events.indices) {
                            desc.append("${model.events[i].provider},")
                        }
                        tvDescription.text = desc
                    }
                }
            }
        }
    }

    fun setData(newList: MutableList<ArticleResponseItem>) {
        this.itemList.clear()
        this.itemList.addAll(newList)
        notifyDataSetChanged()
    }
}