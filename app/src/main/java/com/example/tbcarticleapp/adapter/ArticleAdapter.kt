package com.example.tbcarticleapp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.tbcarticleapp.databinding.RowArticleItemBinding
import com.example.tbcarticleapp.extensions.setImage
import com.example.tbcarticleapp.model.ArticleResponseItem

class ArticleAdapter : RecyclerView.Adapter<ArticleAdapter.ArticleViewHolder>() {

    inner class ArticleViewHolder(val binding: RowArticleItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun onBind() {
            val model = differ.currentList[absoluteAdapterPosition]
            binding.apply {
                tvTitle.text = model.title
                tvUpdateDate.text = model.updatedAt
                ivArticleImage.setImage(model.imageUrl)
                root.setOnClickListener {
                    onItemClick?.let { it(model) }
                }
            }
        }
    }

    private var onItemClick: ((ArticleResponseItem) -> Unit)? = null
    fun setOnItemClick(listener: (ArticleResponseItem) -> Unit) {
        onItemClick = listener
    }

    val differCall = object : DiffUtil.ItemCallback<ArticleResponseItem>() {
        override fun areItemsTheSame(
            oldItem: ArticleResponseItem,
            newItem: ArticleResponseItem
        ): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(
            oldItem: ArticleResponseItem,
            newItem: ArticleResponseItem
        ): Boolean {
            return oldItem == newItem
        }

    }
    val differ = AsyncListDiffer(this, differCall)

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ArticleAdapter.ArticleViewHolder {
        return ArticleViewHolder(
            RowArticleItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ArticleAdapter.ArticleViewHolder, position: Int) {
        holder.onBind()
    }

    override fun getItemCount(): Int = differ.currentList.size
}